﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Net.Mime;
using UnityEngine;
using UnityEngine.UI;

public class Listcourse : MonoBehaviour
{
    public GameObject[] button = new GameObject[3];

    public Animator animDoggo;
    public Animator animKitty;
    public Animator animUnicorn;
    public Animator animParot;

    public void button0()
    {
        button[0].SetActive(false);
        animDoggo.SetTrigger("Direction");
        animKitty.SetTrigger("Direction");
        animUnicorn.SetTrigger("Direction");
        animParot.SetTrigger("Direction");
    }
    
    public void button1()
    {
        button[1].SetActive(false);
        animDoggo.SetTrigger("Direction");
        animKitty.SetTrigger("Direction");
        animUnicorn.SetTrigger("Direction");
        animParot.SetTrigger("Direction");
    }
    
    public void button2()
    {
        button[2].SetActive(false);
        animDoggo.SetTrigger("Direction");
        animKitty.SetTrigger("Direction");
        animUnicorn.SetTrigger("Direction");
        animParot.SetTrigger("Direction");
    }
}

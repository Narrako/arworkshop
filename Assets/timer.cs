﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class timer : MonoBehaviour
{
    private float _time;
    public float timeLeft;
    public int random;
    public bool isClaim;
    public int randomPoint;
    public int pointNumber;
    public bool isPointClaim;

    public Animator animDoggo;
    public Animator animKitty;
    public Animator animUnicorn;
    public Animator animParot;
    
    public GameObject points;
    public GameObject promo;
    
    void Start()
    {
        _time = timeLeft;
    }

    void Update()
    {
        _time -= Time.deltaTime;
        if (_time <= 0) //timer fini 
        {
            _time = timeLeft; // réinitialise le timer
            random = Random.Range(0, 10); //choisi un nombre aléatoire pour choisir entre récompense et promotion
            isClaim = false;
            PlayerPrefs.SetInt("Point", pointNumber);
        }

        if (random < 8 && isClaim == false) //nombre aléatoire choisi pour le gain de point
        {
            animDoggo.SetBool("Fouille", true);
            animKitty.SetBool("Fouille", true);
            animUnicorn.SetBool("Fouille", true);
            animParot.SetBool("Fouille", true);
            points.SetActive(true);
            isClaim = true;
            randomPoint = Random.Range(0, 10); // nombre aléatoire pour les points
            isPointClaim = false;
            StartCoroutine(Wait());
            
            if (randomPoint < 5 && isPointClaim == false) // inférieur a 5 pour le gain faible de point inférieur a 5 pour 50% de chance
            {
                pointNumber += 20;
                isPointClaim = true;
            }

            if (randomPoint >= 5 && isPointClaim == false) // supérieur ou égal a 5 pour le gain haut 
            {
                pointNumber += 30;
                isPointClaim = true;
                
            }
        }

        if (random >= 8 && isClaim == false) // nombre aléatoire choisi pour le gain de promotion
        {
            animDoggo.SetBool("Fouille", true);
            animKitty.SetBool("Fouille", true);
            animUnicorn.SetBool("Fouille", true);
            animParot.SetBool("Fouille", true);
            promo.SetActive(true);
            isClaim = true;
            StartCoroutine(Wait());
        }
    }

    IEnumerator Wait()
    {
        yield return new WaitForSeconds(5);
        animDoggo.SetBool("Fouille", false);
        animKitty.SetBool("Fouille", false);
        animUnicorn.SetBool("Fouille", false);
        animParot.SetBool("Fouille", false);
        promo.SetActive(false);
        points.SetActive(false);
    }
}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class TextInput : MonoBehaviour
{
    GameController controller;
    private string listRecup;
    public TextMeshProUGUI Text;
   public TMP_InputField inputField; //pour utiliser l'inputfield de text mesh pro

    void Awake() //au tout début
    {
       
        controller = GetComponent<GameController>(); //récpuère le composant GameController
        inputField.onEndEdit.AddListener(AcceptStringInput); //quand on édite l'inputfield
        
    }

    void Update()
    {
       
    }

    void AcceptStringInput(string userInput) //pour ce qu'on y écrit
    {
        userInput = userInput.ToLower();
        controller.LogStringWithReturn(userInput);

        char[] delimiterCharacters = { ' ' }; //délimite les characters grace à l'espace entre chaque mot
        string[] separatedInputWords = userInput.Split(delimiterCharacters);
        
        InputComplete(); //lance la fonction InputComplete
    }

    void InputComplete() //quand on renvoit "entrée"
    {
        controller.DisplayLoggedText();
        inputField.text = null;
           
       
    }
}

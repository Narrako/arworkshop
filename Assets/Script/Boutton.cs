﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Boutton : MonoBehaviour
{
    public GameObject listInAR;

    public void ListinAR()
    {
        listInAR.SetActive(true);
    }

    public void Fermer()
    {
        listInAR.SetActive(false);
    }

    public void Home()
    {
        Application.Quit();
    }
    
    public void Debut()
    {
        SceneManager.LoadScene("Start");
    }
    
    public void List()
    {
        SceneManager.LoadScene("List");
    }
    
    public void Shop()
    {
        SceneManager.LoadScene("Shop");
    }
    
    public void Custom()
    {
        SceneManager.LoadScene("Custom");
    }
    
    public void Pet()
    {
        SceneManager.LoadScene("Pet");
    }

    public void Interface()
    {
        SceneManager.LoadScene("Interface");
    }
    
}

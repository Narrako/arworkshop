﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class PointPrint : MonoBehaviour
{
    public int point;

    public string pointPrint;

    public TextMeshProUGUI textPoint;
    // Start is called before the first frame update
    void Start()
    {
      point =  PlayerPrefs.GetInt("Point");
      pointPrint = point.ToString();
      textPoint.text = pointPrint;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}

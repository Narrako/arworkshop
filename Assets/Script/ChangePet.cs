﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangePet : MonoBehaviour
{
    public int number;

    public GameObject doggo;
    public GameObject kitty;
    public GameObject licorne;
    public GameObject parot;
    
    private void Awake()
    {
        number = PlayerPrefs.GetInt("PetNumber");
        
        if (number == 1)
        {
            doggo.SetActive(true);
            kitty.SetActive(false);
            licorne.SetActive(false);
            parot.SetActive(false);
        }
        
        else if (number == 2)
        {
            doggo.SetActive(false);
            kitty.SetActive(false);
            parot.SetActive(false);
            licorne.SetActive(true);
        }
        
        else if (number == 3)
        {
            doggo.SetActive(false);
            kitty.SetActive(true);
            licorne.SetActive(false);
            parot.SetActive(false);
        }
        
        else if (number == 4)
        {
            doggo.SetActive(false);
            kitty.SetActive(false);
            licorne.SetActive(false);
            parot.SetActive(true);
        }
    }
}

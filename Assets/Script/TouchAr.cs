﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TouchAr : MonoBehaviour
{
    public Animator animDoggo;
    public Animator animKitty;
    public Animator animUnicorn;
    public Animator animParot;
    public List<GameObject> arObject;

    void Update()
    {
        RaycastHit hit;
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

        if (Physics.Raycast(ray, out hit))
        {
            foreach(GameObject go in arObject)
            {
                animDoggo.SetBool("Petting", true);
                animKitty.SetBool("Petting", true);
                animUnicorn.SetBool("Petting", true);
                animParot.SetBool("Petting", true);
            }
        }
        else
        {
            animDoggo.SetBool("Petting", false);
            animKitty.SetBool("Petting", false);
            animUnicorn.SetBool("Petting", false);
            animParot.SetBool("Petting", false);
        }
    }
}

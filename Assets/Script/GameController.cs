﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class GameController : MonoBehaviour
{
   public TextMeshProUGUI displayText; //pour pouvoir utiliser text Mesh Pro     

   List<string> actionLog = new List<string>();

    void Start()
    {
        DisplayLoggedText();
    }

    public void DisplayLoggedText()  //tout ce qui est connecté à la pièce
    {
        string logAsText = string.Join("\n", actionLog.ToArray());

        displayText.text = logAsText; //met le texte tapé
    }
    
    public void LogStringWithReturn(string stringToAdd) //passe à la ligne entre chaque action
    {
        actionLog.Add(stringToAdd);
    }
}
